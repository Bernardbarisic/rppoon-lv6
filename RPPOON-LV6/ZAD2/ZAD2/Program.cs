﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Book", 30));
            box.AddProduct(new Product("Headset", 60));
            box.AddProduct(new Product("Sprite", 11));
            IAbstractIterator iterator = box.GetIterator();
            while (iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current);
                iterator.Next();
            }

        }
    }
}
