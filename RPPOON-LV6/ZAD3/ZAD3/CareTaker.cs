﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class CareTaker
    {
        public List<Memento> PreviousStates;

        public void addMemento(Memento memento)
        {
            this.PreviousStates.Add(memento);
        }

        public void removeMemento(Memento memento)
        {
            if (this.PreviousStates.Contains(memento))
            {
                this.PreviousStates.Remove(memento);
            }
        }

        public CareTaker()
        {
            this.PreviousStates = new List<Memento>();
        }

        public CareTaker(List<Memento> mementos)
            : this()
        {
            this.PreviousStates = mementos;
        }

        public Memento getMemento(int index)
        {
            return this.PreviousStates[index];
        }
    }
}
