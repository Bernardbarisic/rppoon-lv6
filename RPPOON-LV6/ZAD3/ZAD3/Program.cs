﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDoItem = new ToDoItem("Gaming set", "Mouse, Headset, Keyboard", DateTime.Now);
            Memento FirstState = toDoItem.StoreState();

            Console.WriteLine(toDoItem.ToString() + "\n");
            toDoItem.ChangeTask("Razer, Zowie, HyperX");
            Memento SecondState = toDoItem.StoreState();

            Console.WriteLine(toDoItem.ToString() + "\n");
            toDoItem.ChangeTask("Gaming, Eat, Sleep");
            Memento ThirdState = toDoItem.StoreState();

            List<Memento> mementos = new List<Memento>() { FirstState, SecondState, ThirdState };
            CareTaker careTaker = new CareTaker(mementos);

            Console.WriteLine(toDoItem.ToString() + "\n");
            toDoItem.RestoreState(careTaker.PreviousStates[1]);
            Console.WriteLine(toDoItem.ToString() + "\n");
            toDoItem.RestoreState(careTaker.PreviousStates[0]);
            Console.WriteLine(toDoItem.ToString() + "\n");
            toDoItem.RestoreState(careTaker.PreviousStates[2]);
            Console.WriteLine(toDoItem.ToString() + "\n");
        }
    }
}
    

