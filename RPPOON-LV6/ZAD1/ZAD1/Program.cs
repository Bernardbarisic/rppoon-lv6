﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Jutro", "Jutarnja tijelovjezba"));
            notebook.AddNote(new Note("Podne", "Rucat"));
            notebook.AddNote(new Note("Poslijepodne", "Druziti se sa prijateljima"));
            notebook.AddNote(new Note("Vecer", "Napravit raspored za sutrasnji dan"));
            Iterator iterator = new Iterator(notebook);
            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
